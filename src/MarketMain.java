import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @Author asha achhami
 * @Date 6/27/2018
 **/
public class MarketMain {
    public static void main(String[] args) {
        ApplicationContext context= new ClassPathXmlApplicationContext("Spring.xml");
//prototype
        Market ob1= (Market)context.getBean("marketbean");
        ob1.setSales("hello customer");
        ob1.welcome();

        Market ob2=(Market)context.getBean("marketbean");
        //ob2.setSales("sdcsad");
        ob2.welcome();

//for singletone scope
        Market o1= (Market)context.getBean("marketbean1");
        o1.setSales("hello customer");
        o1.welcome();

        Market o2=(Market)context.getBean("marketbean1");
        o2.welcome();


    }
}
